﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using XamlAnimatedGif;

namespace MediaPlayer
{
    /// <summary>
    /// Interaction logic for UpdatingScreen.xaml
    /// </summary>
    public partial class UpdatingScreen : Window
    {
        public UpdatingScreen()
        {
            InitializeComponent();
            AnimationBehavior.SetSourceUri(Spinner, new Uri(@"file://C:\git\imcsclient\MediaPlayer\MediaPlayer\Resources\ajax_loader_gray_512.gif"));
            AnimationBehavior.SetRepeatBehavior(Spinner, RepeatBehavior.Forever);
            AnimationBehavior.SetAutoStart(Spinner, true);
        }
    }
}
