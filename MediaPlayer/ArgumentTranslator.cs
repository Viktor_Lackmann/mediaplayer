﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayer
{
    /// <summary>
    /// Übersetzer der Start Argumente
    /// </summary>
    public static class ArgumentTranslator
    {
        /// <summary>
        /// Erstellt ein MediaFile-Objekt mit Start- und Stopzeit
        /// </summary>
        /// <param name="argumentedPath">String mit Pfad und weiteren Argumenten</param>
        /// <returns>MediaFile-Objekt mit Start- und Stopzeit</returns>
        public static MediaFile Get_MediaFile_WithStats(string argumentedPath)
        {
            string path;
            TimeSpan start = new TimeSpan(0, 0, 0); 
            TimeSpan stop = new TimeSpan(0, 0, 0);

            string[] args = argumentedPath.Split('|');
            path = args[0].TrimEnd(' ');

            for (int i = 1; i < args.Length; i++)
            {
                if (args[i].TrimStart(' ').StartsWith("start"))
                {
                    TimeSpan.TryParse(args[i].Split('=')[1], out start);
                }
                if (args[i].TrimStart(' ').StartsWith("stop"))
                {
                    TimeSpan.TryParse(args[i].Split('=')[1], out stop);
                }
            }

            return new MediaFile(path, start);
        }
    }
}
