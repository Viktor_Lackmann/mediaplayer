﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Controls;
using System.Threading;
using System.Windows.Threading;

namespace MediaPlayer
{
    /// <summary>
    /// Instanziert ein neues MediaFile-Objekt
    /// </summary>
    public class MediaFile
    {
        public delegate void TimerIsOverDelegate();
        public event TimerIsOverDelegate StopFileStreamEvent;

        #region Membervariablen;
        //DispatcherTimer _runtime;

        string _name;
        string _path;
        string _extension;

        TimeSpan _start;
        #endregion

        #region Konstruktoren
        /// <summary>
        /// Instanziert ein neues Mediafile-Objekt
        /// </summary>
        /// <param name="args">Pfad zur Datei</param>
        public MediaFile(string args)
        {
            _path = args.Split('|')[0].TrimEnd(' ');
            _start = new TimeSpan(0, 0, 0);

            initialize();
        }


        public MediaFile(string args, TimeSpan start)
        {
            _path = args.Split('|')[0].TrimEnd(' ');
            _start = start;

            initialize();
        }

        #endregion


        //private void On_Tick(object sender, EventArgs e)
        //{
        //    TimerIsOver();
        //}

        #region private Methods
        /// <summary>
        /// Initialisiert das MediaFile-Objekt
        /// </summary>
        /// <returns>Gibt zurück, ob die Aktion funktioniert hat</returns>
        private bool initialize()
        {
            FileInfo fileInfo = new FileInfo(_path);
            _name = fileInfo.Name;
            _path = fileInfo.FullName;
            _extension = fileInfo.Extension;

            return true;
        }
        #endregion

        #region public Getter, Setter

        /// <summary>
        /// Setzt, oder returned den Namen der Datei
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Gibt de Pfad zur Datei zurück
        /// </summary>
        public string Path
        {
            get { return _path; }
        }

        /// <summary>
        /// Gibt die Start-Zeit zurück
        /// </summary>
        public TimeSpan StartZeit
        {
            get { return _start; }
        }

        /// <summary>
        /// Gibt die Extension der Datei zurück
        /// </summary>
        public string Extension
        {
            get { return _extension; }
        }

        //public DispatcherTimer Runtime
        //{
        //    get { return _runtime; }
        //}
        #endregion

        public bool Play()
        {
            return false;
        }

        public bool Pause()
        {
            return false;
        }

        public bool Stop()
        {
            return false;
        }
    }
}
