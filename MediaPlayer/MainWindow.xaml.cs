﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MediaPlayer
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string[] args;
        private bool isPlaying;
        private bool shortcuts_Activated;

        Player pl;

        public MainWindow()
        {
            args = App.mArgs;
            InitializeComponent();

            #region testing
            //string teststring = String.Join("", args.Select(x => x));
            //MessageBox.Show(teststring);
            //Clipboard.SetText(teststring);
            #endregion

            pl = new Player(meLayer, imgTemp, args);
            pl.Play();
        }
        public MainWindow(string[] args)
        {
            InitializeComponent();
            pl = new Player(meLayer, imgTemp, args);
            pl.Play();
        }
        

        private void meLayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (pl.Loop)
            {
                pl.play_NextFile();
            }
            else
            {
                Close();
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            //bool shortcuts_Activated = false;


            if (e.Key == Key.F12)
            {
                if (!shortcuts_Activated)
                    shortcuts_Activated = true;
                else
                    shortcuts_Activated = false;
            }

            if (!shortcuts_Activated)
                return;

            if (e.Key == Key.M)
            {
                if (pl.Mute)
                    pl.Set_Unmute();
                else
                    pl.Set_Mute();
            }

            if (e.Key == Key.P)
            {
                if (isPlaying)
                {
                    meLayer.Pause();
                    isPlaying = false;
                }
                else
                {
                    meLayer.Play();
                    isPlaying = true;
                }
            }
            if (e.Key == Key.A)
            {
                if (!pl.play_LastFile())
                    Close();
                isPlaying = true;
            }
            if (e.Key == Key.D)
            {
                if (!pl.play_NextFile())
                    Close();
                isPlaying = true;
            }

            if (e.Key == Key.Escape)
                this.Close();
        }

        
    }
}
