﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Threading;
using System.Threading;

namespace MediaPlayer
{
    /// <summary>
    /// Instanziert ein neues Player-Objekt
    /// </summary>
    public class Player
    {
        #region Member
        MediaElement _mePlayer;
        MediaFile _actualFile;
        Image imgTemp;
        
        int _actualFile_Index;
        bool _loop = true;
        bool _mute = true;

        /// <summary>
        /// Liste der aktuellen Wiedergabe
        /// </summary>
        List<MediaFile> _mediaFiles = new List<MediaFile>();
        #endregion



        #region Konstruktor
        /// <summary>
        /// Instanziert ein neues Player-Objekt
        /// </summary>
        /// <param name="player">MediaElement-Instanz</param>
        /// <param name="imgTemp">Temporäres Bild während MP3-Wiedergabe</param>
        /// <param name="args">Array mit den wiederzugebenen Medien</param>
        public Player(MediaElement player, Image imgTemp, string[] args)
        {
            _mePlayer = player;
            initializePlayer();
            Set_PlayerDataSource(args.ToList<string>());

            Set_Mute();

            this.imgTemp = imgTemp;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Initialisiert den Player
        /// </summary>
        private void initializePlayer()
        {
            _mePlayer.LoadedBehavior = MediaState.Manual;
            _mePlayer.UnloadedBehavior = MediaState.Manual;
        }
        #endregion

        #region Public Methods/Getter/Setter
        /// <summary>
        /// Gibt den Titel des aktuell laufenden Medium aus
        /// </summary>
        public string OverlayText
        {
            get
            {
                return _actualFile == null ? "Keine Datei geladen" : _actualFile.Name;
            }
        }

        /// <summary>
        /// Boolischer Wert, gibt zurück, ob der Player die Dateien Loopen soll
        /// </summary>
        public bool Loop
        {
            get { return _loop; }
        }

        public bool Mute
        {
            get { return _mute; }
            set { _mute = value; }
        }

        public object File_StopsManually { get; private set; }

        /// <summary>
        /// Füllt die Wiedergabeliste mit Medien
        /// </summary>
        /// <param name="args">Liste mit Medien-Namen</param>
        /// <returns>Gibt zurück, ob die Aktion funktioniert hat</returns>
        public bool Set_PlayerDataSource(List<string> args)
        {
            try
            {
                for (int i = 0; i < args.Count; i++)
                {
                    string[] pathWithArguments = args[i].Split('|');

                    if (pathWithArguments.Length > 1)
                        _mediaFiles.Add(ArgumentTranslator.Get_MediaFile_WithStats(args[i]));
                    else
                        _mediaFiles.Add(new MediaFile(args[i]));
                }
                _actualFile = _mediaFiles[0];
                _actualFile_Index = 0;

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Startet die aktuelle Datei
        /// </summary>
        /// <returns>Gibt zurück, ob die Aktion funktioniert hat</returns>
        public bool Play()
        {
            if (_actualFile.Extension == ".mp3")
                imgTemp.Visibility = System.Windows.Visibility.Visible;
            else
                imgTemp.Visibility = System.Windows.Visibility.Hidden;
            
            _mePlayer.Source = new Uri(_actualFile.Path);
            _mePlayer.Position = _actualFile.StartZeit;

            _mePlayer.Play();
            
            return true;
        }

        /// <summary>
        /// Wechselt zur nächsten Datei
        /// </summary>
        /// <returns>Gibt zurück, ob die Aktion funktioniert hat</returns>
        public bool play_NextFile()
        {
            _actualFile_Index++;

            if (_actualFile_Index >= _mediaFiles.Count)
            {
                if (_loop)
                    _actualFile_Index = 0;
                else
                    _actualFile_Index = -1;
            }

            if (_actualFile_Index == -1)
                return false;

            _actualFile = _mediaFiles[_actualFile_Index];

            Play();

            return true;
        }

        /// <summary>
        /// Wächselt zur vorheriger Datei
        /// </summary>
        /// <returns>Gibt zurück, ob die Aktion funktioniert hat</returns>
        public bool play_LastFile()
        {
            _actualFile_Index--;

            if (_actualFile_Index < 0)
            {
                if (_loop)
                    _actualFile_Index = _mediaFiles.Count - 1;
                else
                    _actualFile_Index = -1;
            }

            if (_actualFile_Index == -1)
                return false;
            
            _actualFile = _mediaFiles[_actualFile_Index];

            Play();

            return true;
        }

        public bool Set_Mute()
        {
            if (_mePlayer.Volume >= 0.0d)
                _mePlayer.Volume = 0.0d;

            _mute = true;

            return true;
        }

        public bool Set_Unmute()
        {
            if (_mePlayer.Volume == 0)
                _mePlayer.Volume = 0.5d;

            _mute = false;

            return true;
        }
        #endregion
    }
}
